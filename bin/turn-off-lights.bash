#!/usr/bin/env bash

# ==============================================================================
# I'm not sure why, but rebooting the Pi turns the LEDs back on when I
# explicitly turn them off in the baby-monitor.service.  This script just helps
# me reset the LEDs when I need to.
# ==============================================================================

declare -a LEDS="$(ls /sys/class/leds)"
for LED in ${LEDS[@]}; do
    echo -n "Setting ${LED} trigger to "
    echo none | sudo tee /sys/class/leds/${LED}/trigger
done

declare -a BRIGHTNESS=( 1 0 )
for VALUE in ${BRIGHTNESS[@]}; do
    for LED in ${LEDS[@]}; do
        echo -n "Setting ${LED} brightness to "
        echo ${VALUE} | sudo tee /sys/class/leds/${LED}/brightness
    done
    sleep 10
done

for LED in ${LEDS[@]}; do
    echo -n "Value for ${LED}: "
    cat /sys/class/leds/${LED}/brightness
done

#echo 1 | sudo tee /sys/class/leds/led0/brightness
#echo 1 | sudo tee /sys/class/leds/led1/brightness
#echo 0 | sudo tee /sys/class/leds/led0/brightness
#echo 0 | sudo tee /sys/class/leds/led1/brightness
