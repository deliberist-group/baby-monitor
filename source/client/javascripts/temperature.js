function get_temperature() {
  fetch('/temperature', {
    method: 'GET',
  }).then(function(response) {
    return response.json();
  }).then(function (json_resp) {
    const elem = document.getElementById('temperature');
    elem.innerText = '';
    elem.innerText += `Celsius: ${json_resp.celsius}`;
    elem.innerText += ', ';
    elem.innerText += `Fahrenheit: ${json_resp.fahrenheit}`;
  }).catch(function(err) {
    console.log(`Error: ${err}`);
  });
}

get_temperature();
setInterval(get_temperature, 60000);
