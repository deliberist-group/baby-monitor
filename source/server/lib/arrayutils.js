// // TODO docs
// module.exports.getRandom = function (arr, n) {
//     const result = new Array(n);
//     const len = arr.length;
//     const taken = new Array(len);
//     if (n > len) {
//         throw new RangeError('_getRandom: more elements taken than available');
//     }
//     while (n--) {
//         let x = Math.floor(Math.random() * len);
//         result[n] = arr[x in taken ? taken[x] : x];
//         taken[x] = --len in taken ? taken[len] : len;
//     }
//     return result;
// };

/**
 * Shuffles array in place. ES6 version
 * @param {Array} items An array containing the items.
 */
module.exports.shuffle = function (items) {
    // TODO change this so it does not shuffle in place, but return a new array.
    const shuffledArray = items.slice();
    for (let i = shuffledArray.length - 1; i >= 0; i--) {
        const randomIndex = Math.floor(Math.random() * (i + 1));
        const itemAtIndex = shuffledArray[randomIndex];
        shuffledArray[randomIndex] = shuffledArray[i];
        shuffledArray[i] = itemAtIndex;
    }
    return shuffledArray;
};
